##### VMs Part #####

resource "yandex_compute_instance" "sonar" {
  name        = "sonar"
  platform_id = "standard-v2"
  resources {
    core_fraction = 50
    cores         = "4"
    memory        = "4"
  }
  boot_disk {
    initialize_params {
      image_id = "fd8evlqsgg4e81rbdkn7" # ubuntu-22
      size     = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta2.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

##### Create file inventory #####

resource "local_file" "invsonar" {
  content  = <<EOF

[sonar]
${yandex_compute_instance.sonar.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa

[all]
${yandex_compute_instance.sonar.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa
EOF
  filename = "${path.module}/invsonar"
}

##### Provisioning with Ansible VM sonar #####

resource "null_resource" "sonar" {
  depends_on = [yandex_compute_instance.sonar, local_file.invsonar]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.sonar.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i invsonar --key-file ansible.key pb-sonar.yml"
  }
}

##### Add A-records to DNS zone #####

resource "yandex_dns_recordset" "sonar_dns_name" {
  depends_on = [yandex_compute_instance.sonar]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "sonar"
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.sonar.network_interface.0.nat_ip_address]
}
