variable "ssh_credentials" {
  description = "Credentials for connect to instances"
  type = object({
    user        = string
    private_key = string
    pub_key     = string
  })
  default = {
    user        = "ubuntu"
    private_key = "ansible.key"
    pub_key     = "~/.ssh/id_rsa.pub"
  }
}

